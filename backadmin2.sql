/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.22 : Database - backadmin2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`backadmin2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `backadmin2`;

/*Table structure for table `file` */

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `id` varchar(32) COLLATE utf8_bin NOT NULL,
  `file_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '文件名',
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '下载路径',
  `size` bigint(20) DEFAULT NULL COMMENT '文件大小',
  `upload_time` timestamp NULL DEFAULT NULL COMMENT '上传时间',
  `status` int(1) DEFAULT NULL COMMENT '0代表可用；1代表删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `file` */

insert  into `file`(`id`,`file_name`,`url`,`size`,`upload_time`,`status`) values ('06bddf552d4541619533955f690a4f48','Java Web知识框架.xmind','http://www.qiniu.file.yujiago.cn/52a8572c-3c7f-47e3-9917-75a98f1d25f1.xmind',141,'2018-09-01 11:08:59',0),('0cb494cbe790476abc3d6bbd5ad053b1','css与javascript笔记.doc','http://www.qiniu.file.yujiago.cn/47487bc6-fa28-47c9-a93e-1a32e5d83326.doc',492,'2018-08-15 19:13:26',0),('0df96550f8c14f98ac82c3533a4b6685','给还没有毕业孩子的一些忠告_3.docx','http://www.qiniu.file.yujiago.cn/b412cceb-7504-444e-bf33-1a3166bb7d9a.docx',25,'2018-08-18 20:54:34',0),('0fd2786ef5f14f1797d5024a76fb0796','摄影笔记.docx','http://www.qiniu.file.yujiago.cn/65b91eb3-9e96-4964-9a2d-3998e17d5302.docx',1358,'2018-08-19 15:29:51',0),('149da4b2fc054aeca8ad23fcc407e80b','2015软工班编译原理期中试卷答案.pdf','http://www.qiniu.file.yujiago.cn/a3e955fc-9b4d-4175-a5c2-95ec72bcb3cb.pdf',91,'2018-08-15 19:01:43',0),('153a42e288714643a97145bc09923809','css&js.xmind','http://www.qiniu.file.yujiago.cn/83689aea-3867-45e5-9c58-1ec88051cb28.xmind',53,'2018-08-15 17:29:37',0),('185592b91ab6466eb338c23e23e463c2','西方哲学论文.docx','http://www.qiniu.file.yujiago.cn/3871685e-f607-412f-9d33-f2f211d49a88.docx',20,'2018-08-15 17:29:37',0),('2c12fe8e3a604930926526217b862d67','CSS2.0样式表中文手册.chm','http://www.qiniu.file.yujiago.cn/6bff423f-9d68-448f-8b4d-089ac3b832a7.chm',553,'2018-08-15 19:01:43',0),('2f4032a3c9734161926b217ccedc140c','给还没有毕业孩子的一些忠告_2.docx','http://www.qiniu.file.yujiago.cn/c7f8ee30-e9b6-4967-a392-ccbe0a05fd3a.docx',24,'2018-08-18 20:54:34',0),('47b37f4f31f244758b3b421dfbdf7b05','致你终将逝去的二十岁.docx','http://www.qiniu.file.yujiago.cn/150d40a2-078c-4f8f-b473-5cdf6ef40cd4.docx',16,'2018-08-18 21:20:13',0),('6c6c23ebc8464bfeb4d62c0a0bbd7d5c','html思维导图.xmind','http://www.qiniu.file.yujiago.cn/97b7be29-b841-415d-a374-eaec29953af6.xmind',173,'2018-08-15 19:13:26',0),('710476fcf6744a94b82df076b6d1352f','英伦对决.mkv.torrent','http://www.qiniu.file.yujiago.cn/65725e0e-2064-4782-9795-d33d26b87d09.torrent',17,'2018-08-18 20:04:46',0),('8d9c678f53c642ab917601f12f5d27cc','SQL语句.doc','http://www.qiniu.file.yujiago.cn/b4453bfc-cfeb-4ad9-997d-4b3c97f48725.doc',455,'2018-08-15 18:57:31',0),('b590dc0252734dfabe52d67d06d712d1','html笔记.doc','http://www.qiniu.file.yujiago.cn/b0c15218-6d18-4034-9aca-6c760f2d7a9c.doc',883,'2018-08-15 19:13:26',0),('c34fc6e3123f4466b4aa64f7c5c9d975','尚硅谷Spring4、SpringMVC、MyBatis视频教程.txt','http://www.qiniu.file.yujiago.cn/03d9d30a-1e5a-465f-8444-d81428f1b43d.txt',0,'2018-08-20 21:54:01',0),('c5569848655f4ca3b5484dc82888de6e','给还没有毕业孩子的一些忠告_4.docx','http://www.qiniu.file.yujiago.cn/a1bae920-ff44-4645-aec7-e46fb5f6d1b7.docx',21,'2018-08-18 20:54:34',0),('c865ec28774b4bc6a9fcb883ea358422','给还没有毕业孩子的一些忠告_1.docx','http://www.qiniu.file.yujiago.cn/6d5c983e-c913-4ab8-9f62-fecda3b27553.docx',22,'2018-08-18 20:54:34',0),('c8a32622d3c344a19baca26f16c9446a','网络协议.xmind','http://www.qiniu.file.yujiago.cn/eb16235f-5501-40f7-a9c9-adfc1e21b586.xmind',41,'2018-08-15 19:42:47',0),('cb6c67cfdf0649acad304fb0544bbd83','jdbc笔记.doc','http://www.qiniu.file.yujiago.cn/d615b4c1-30b8-405f-a63f-777cabf48a41.doc',242,'2018-08-15 18:52:09',0),('d0c779e6d0a34cb3a4780b1bff284e4b','html笔记.doc','http://www.qiniu.file.yujiago.cn/a409261b-dd07-4a1e-aa96-7b01a48e4b3f.doc',883,'2018-08-15 18:57:31',0),('eb556d7ca04543dcbcb5ce1c526767fe','UML六大关系图解.png','http://www.qiniu.file.yujiago.cn/fe78b1d0-c2ca-4006-911b-828a6010a080.png',561,'2018-08-15 17:29:37',0),('ebd537942c054a68a3452d1380e92b1b','java工具使用问题.docx','http://www.qiniu.file.yujiago.cn/701811b5-ad17-4108-b7b9-3d71ddbb6a96.docx',28,'2018-08-15 18:52:09',0),('f15b564e1d024588937222d2f648c8e4','读大学，究竟读什么.doc','http://www.qiniu.file.yujiago.cn/672c1c44-739e-4f90-956c-e1100bcdddd0.doc',31,'2018-08-18 21:09:37',0);

/*Table structure for table `grade` */

DROP TABLE IF EXISTS `grade`;

CREATE TABLE `grade` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `gradeIcon` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `gradeName` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `gradeValue` int(16) DEFAULT NULL,
  `gradeGold` int(16) DEFAULT NULL,
  `gradePoint` int(16) DEFAULT NULL,
  `gradeStatus` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `grade` */

insert  into `grade`(`id`,`gradeIcon`,`gradeName`,`gradeValue`,`gradeGold`,`gradePoint`,`gradeStatus`) values (1,'icon-vip1','倔强青铜',0,0,0,'checked'),(2,'icon-vip2','秩序白银',500,200,100,'checked'),(3,'icon-vip3','荣耀黄金',1000,300,300,'checked'),(4,'icon-vip4','尊贵铂金',2000,400,800,'checked'),(5,'icon-vip5','永恒钻石',5000,500,1500,'checked'),(6,'icon-vip6','至尊星耀',10000,600,3000,'checked'),(7,'icon-vip7','最强王者',20000,700,5000,'checked'),(8,'icon-vip8','宇宙王者',30000,800,8000,'checked'),(9,'icon-vip9','超级宇宙王者',50000,900,10000,'checked');

/*Table structure for table `image` */

DROP TABLE IF EXISTS `image`;

CREATE TABLE `image` (
  `id` varchar(32) COLLATE utf8_bin NOT NULL,
  `title` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `isShow` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `image` */

insert  into `image`(`id`,`title`,`remark`,`url`,`isShow`) values ('0019d73641694aa5809a02e7323ab45c','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/IMG_1157_1-%E7%BC%96%E8%BE%91.jpg',1),('001ef4d7e5e646048c8af9729a464785','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/1.jpg',1),('01615b284df14f84b2bc6068ec37eefd','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/2018-01-06_134831.png',1),('030c090d85944a0faf0d66683b99b67f','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/d98c7104-c229-4fc3-a2c2-7f5e26938238',0),('10da4a7deb9347e980ec47b98bde33d2','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/3278c50f-8527-4eee-ab00-baa6e9c01358',0),('1e3960c640d44221a92f2d328b544b5c','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/2018-01-21_153917.png',0),('23c2f67afd2b409f8ff24e0134fd73a6','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/1bbd740d-9216-411d-9332-d6cce0f2923e',0),('25dd4d57a8ac4f00950f7a9950d9f570','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/d7fa46c5-19d0-4332-8d4c-095cd61f08a1',0),('2c2ba36fafaa4546b83adac730b91f2c','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/9e6d7f61-861c-4e2b-adee-752239fd3647',0),('3786d9b8ce6644289c6d2ad6098187eb','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/041dd730-193c-4640-a40c-96a6a6ede796',0),('3f21bec6c9884e98832c4dbe47a202c2','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/dc5acd0f-3157-4a7f-a727-e82104970606.jpg',0),('479720483bf24fb3802cedd4528f15da','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/2018-01-23_195116.png',0),('5659c2dae58a4c239cea4c020cba4082','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/2018-02-25_210651.png',0),('573fb87486834931aef761ec3d2f9cd6','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/b6242c75-33bb-4eb4-ad5f-0fd22c307c0b',0),('5ca8a623261b4f2d95d64712492f8394','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/2018-02-28_175332.png',0),('5e3e96fad07e4615bcb673b5122401ec','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/2018-03-21_130359.png',0),('6bcf606eaee64326babfd7e127b57f7f','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/d26a1411-664e-49f9-8a3d-0b2146096a146.png',0),('6ccb95afff624682a620e4aefcf60cd3','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/a111c533-c4b6-4e04-87b8-5d62a40b0d05',0),('72a3a54c1d494dbbbb937843ac73acfe','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/2018-04-06_152834.png',0),('7b860e00abfd430eb20a792a1082dc50','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/2493d477-c4c1-44ed-82b9-6608a0416172',0),('7c54b4f5f9d546208d9a591c03ddb222','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/2018-04-07_202406.png',0),('838a0f06b069438e8a0b01b583d3fa2c','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/IMG_1157_1-%E7%BC%96%E8%BE%91.jpg',0),('840891c3a769447e9977a96d2638e7d7','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/ea9cf415-6f5c-4b34-90e4-f4c2fd1d81d1',0),('8500312966f84999934ff99d145faee9','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/bcef5f89-a37e-4aa6-9a15-5cfdc29aea9a',0),('876f249a81ec47599dd5f44fe04e3a83','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/5dcca070-9455-46a2-8605-b9153d16ea35',0),('87cda69c78c64bf3bdd229572c617160','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/360wallpaper.jpg',1),('8c5a85cf4aaa4b0187818686614d9104','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/IMG_1157_1-%E7%BC%96%E8%BE%91.jpg',1),('92ece88e2a7d499cb8bde52c93c997d6','Qiniu',NULL,'http://pd0t7ruwr.bkt.clouddn.com/IMG_1157_1-%E7%BC%96%E8%BE%91.jpg',1),('937a3366176b4cd0b9207922345d9ebb','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/22fdd025-9b48-4d4d-9597-1bb7a632f5da',0),('976173720c654a268366fab71668bd8c','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXWiAO2RJAAiYzhG_eqM139.png',1),('99b98fc8ffb34282a74d950e7c800820','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXTeAPcnGAAO-m19cF3I300.png',1),('9b5fee4b5189465daaece0a1e9656028','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXU-AGfkdAApVJnAFE2Q890.png',1),('a135a877f2e2423e903200510eaffc8b','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/dc705b5d-c0a0-4334-b41e-d7666f93b78b',1),('a4f7e57083d94a2a9407f07b7f570d91','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXWeAdL9CAARxOXbvyNo287.png',1),('a9f45c5d9e114d469015a9462b25e7b2','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXTWAR7keAAeRZeBsmEE210.png',1),('a9f49948dfd944a2b04614b6eb4265dd','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/d85a0184-9fae-463c-beda-5a3cfa0e33d3.jpg',1),('acd0c80e96ab4eada2dd983bc32553b7','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXWGAegpiAAYeeDFwsWI852.png',1),('b3c7d010c4434acd8f72844c06e726d8','Qiniu',NULL,'http://www.qiniu.image.yujiago.cn/9a0653a1-8a08-4b15-bf35-94414ae44a19',0),('b4c5edb54fb04eefb82c5c63e8bf8352','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXSuARWOsAAn9J0mAc8A944.png',1),('b6a5eb8cabbe42db975267f20eaf6c2a','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXVmAGRo8AA2fxm_cmnM647.png',1),('c534b20d55ea486c9a8bea71bfe5d38d','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/d3cdc201-de95-436e-bc69-8d6a0fa49377',0),('c58bf7107d7e4f4296477cc734f3dad7','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXXOAGCYOAAtF9R-NCwo192.png',1),('ca78664a12da4a7a8495a46fa5f0bd45','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXTiARjVQAAZLUzMIj5g051.png',1),('d45629fe1e474a0fb3fb9fad891137eb','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/a33f22fb-98b3-4a98-86bc-672198bbb5ab',0),('d563a901202a42f787666175aeeb23e9','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXUaAWzLjAA7FgaUUoa8579.png',1),('df2c14805f0f4023be2a5cb6aefba188','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXWOAaDfxAAny8s2sXRQ658.png',1),('e1daa8fa9e5b4f809685566d3dc01aaf','QINIU',NULL,'http://www.qiniu.image.yujiago.cn/a34a16e5-74a3-4d64-93ca-85689a076dc2',0),('eed6c70cdabf49dbb81773f294d9d7e4','FastDFS',NULL,'http://www.image.yujiago.cn:89/group1/M00/00/00/rBD8pFtcXP2AbA-CAAl6xd0mbjU087.png',1);

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` varchar(32) COLLATE utf8_bin NOT NULL,
  `news_name` varchar(128) COLLATE utf8_bin NOT NULL COMMENT '标题',
  `news_abstract` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '概要',
  `news_content` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '内容',
  `classify` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '分类',
  `news_status` tinyint(1) DEFAULT NULL COMMENT '发布状态，0草稿，1待审核，2审核通过',
  `news_time` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '创建时间',
  `news_look` tinyint(1) DEFAULT NULL COMMENT '浏览权限，0开放，1私密',
  `news_top` varchar(8) COLLATE utf8_bin DEFAULT NULL COMMENT '是否置顶，true/false',
  `news_author_id` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '作者ID',
  PRIMARY KEY (`id`),
  KEY `new_admin` (`news_author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `news` */

insert  into `news`(`id`,`news_name`,`news_abstract`,`news_content`,`classify`,`news_status`,`news_time`,`news_look`,`news_top`,`news_author_id`) values ('2c9ba1816368f87a016368f9c6690000','css3用transition实现边框动画效果','css3用transition实现边框动画效果css3用transition实现边框动画效果','css3用transition实现边框动画效果<img src=\'../../images/userface1.jpg\' alt=\'文章内容图片\'>css3用transition实现边框动画效果css3用transition实现边框动画效果','notice ',0,'2018-05-16 20:42:39',0,'checked','1'),('2c9ba1816368f87a016368f9c6690001','自定义的模块名称可以包含/吗','自定义的模块名称可以包含/吗自定义的模块名称可以包含/吗','自定义的模块名称可以包含自定义的模块名称可<img src=\'../../images/userface2.jpg\' alt=\'文章内容图片\'>以包含自定义的模块名称可以包含自定义的模块名称可以包含','notice ',1,'2018-05-16 20:42:39',1,'','2'),('2c9ba18163692a8501636939594e0000','layui.tree如何ajax加载二级菜单','layui.tree如何ajax加载二级菜单layui.tree如何ajax加载二级菜单','layui.tree如何ajax加载二级菜单layui.tree如何<img src=\'../../images/userface3.jpg\' alt=\'文章内容图片\'>ajax加载二级菜单layui.tree如何ajax加载二级菜单','images ',2,'2018-05-16 21:52:16',1,'checked','1'),('2c9ba18163692a850163693a27a80001','layui.upload如何带参数？像jq的data:{}那样','layui.upload如何带参数？像jq的data:{}那样layui.upload如何带参数？像jq的data:{}那样','layui.upload如何带参数？像jq的data:{}那样layui.upload如何带参数？像jq的data:{}那样layui.upload如何带参数？像jq的data:{}那样',NULL,1,'2018-05-17 15:23:17',1,'checked','2'),('2c9ba18163692a850163693a583d0002','表单元素长度应该怎么调整才美观','表单元素长度应该怎么调整才美观表单元素长度应该怎么调整才美观','表单元素长度应该怎么调整才美观表单元素长度应该怎么调整才美观表单元素长度应该怎么调整才美观表单元素长度应该怎么调整才美观',NULL,0,'2018-05-16 21:53:35',0,'checked','1'),('2c9ba18163692a850163693aa1ee0003','layui 利用ajax冲获取到json 数据后 怎样进行渲染','layui 利用ajax冲获取到json 数据后 怎样进行渲染layui 利用ajax冲获取到json 数据后 怎样进行渲染','layui 利用ajax冲获取到json 数据后 怎样进行渲染layui 利用ajax冲获取到json 数据后 怎样进行渲染layui 利用ajax冲获取到json 数据后 怎样进行渲染',NULL,2,'2018-05-16 21:53:54',1,'','2'),('2c9ba18163692a850163693ac53b0004','微信页面中富文本编辑器LayEdit无法使用','微信页面中富文本编辑器LayEdit无法使用微信页面中富文本编辑器LayEdit无法使用','微信页面中富文本编辑器LayEdit无法使用微信页面中富文本编辑器LayEdit无法使用微信页面中富文本编辑器LayEdit无法使用',NULL,1,'2018-05-16 21:54:02',1,'checked','2'),('2c9ba18163692a850163693ae98a0005','layui 什么时候发布新的版本呀','layui 什么时候发布新的版本呀layui 什么时候发布新的版本呀','layui 什么时候发布新的版本呀layui 什么时候发布新的版本呀layui 什么时候发布新的版本呀layui 什么时候发布新的版本呀',NULL,0,'2018-05-16 21:54:15',0,'checked','2'),('2c9ba18163692a850163693af8cf0006','layui上传组件不支持上传前的图片预览嘛？','layui上传组件不支持上传前的图片预览嘛？layui上传组件不支持上传前的图片预览嘛？','layui上传组件不支持上传前的图片预览嘛？layui上传组件不支持上传前的图片预览嘛？layui上传组件不支持上传前的图片预览嘛？',NULL,1,'2018-05-16 21:54:20',1,'','2'),('2c9ba18163692a850163693b1e750007','关于layer.confirm点击无法关闭的疑惑','关于layer.confirm点击无法关闭的疑惑关于layer.confirm点击无法关闭的疑惑','关于layer.confirm点击无法关闭的疑惑关于layer.confirm点击无法关闭的疑惑关于layer.confirm点击无法关闭的疑惑',NULL,0,'2018-05-16 21:54:24',0,'','1'),('2c9ba18163692a850163693b32fd0008','layui form表单提交成功如何拿取返回值','layui form表单提交成功如何拿取返回值layui form表单提交成功如何拿取返回值','layui form表单提交成功如何拿取返回值layui form表单提交成功如何拿取返回值layui form表单提交成功如何拿取返回值',NULL,0,'2018-05-16 21:54:34',0,'checked','2'),('2c9ba181636d1b2401636d1b63590000','layer mobileV2.0 yes回调函数无法用？','layer mobileV2.0 yes回调函数无法用？layer mobileV2.0 yes回调函数无法用？','layer mobileV2.0 yes回调函数无法用？layer mobileV2.0 yes回调函数无法用？layer mobileV2.0 yes回调函数无法用？',NULL,0,'2018-05-17 15:58:14',0,'','1');

/*Table structure for table `sys_dept` */

DROP TABLE IF EXISTS `sys_dept`;

CREATE TABLE `sys_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '0:正常，1:删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='部门管理';

/*Data for the table `sys_dept` */

insert  into `sys_dept`(`id`,`parent_id`,`name`,`order_num`,`del_flag`) values (1,0,'研发部',0,0),(2,1,'研发一部',0,0),(3,1,'研发二部',1,0),(4,0,'销售部',1,0),(5,0,'产品部',2,0),(6,5,'产品一部',0,0),(7,0,'测试部',3,0),(8,7,'测试一部',0,0),(9,7,'测试二部',1,0),(10,4,'销售一部',0,0);

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`id`,`parent_id`,`name`,`url`,`perms`,`type`,`icon`,`order_num`,`gmt_create`,`gmt_modified`) values (1,0,'内容管理','javascript:;','con:view',0,'icon-text',1,'2017-08-09 22:49:47',NULL),(2,0,'权限管理','javascript:;','prem:view',0,'icon-text',2,'2017-08-09 22:55:15',NULL),(3,0,'系统设置','javascript:;','sys:view',0,'icon-text',3,'2017-08-09 23:06:55',NULL),(4,0,'数据图表','javascript:;','data:view',0,'icon-text',4,'2017-08-10 14:12:11',NULL),(5,0,'使用文档','javascript:;','doc:view',0,'icon-text',5,'2017-08-10 14:13:19',NULL),(6,1,'图片管理','/page/content/images','con:pic:view',1,'icon-text',1,'2017-08-14 10:51:35',NULL),(7,1,'文章管理','/page/content/newsList','con:news:view',1,'icon-text',2,'2017-08-14 10:52:06',NULL),(8,1,'文件管理','/page/content/fileList','con:file:view',1,'icon-text',3,'2017-08-14 10:52:24',NULL),(9,2,'菜单管理','/page/perm/menuList','prem:menu:view',1,'icon-text',1,'2017-08-14 10:56:37',NULL),(10,2,'角色管理','/page/perm/roleList','prem:role:view',1,'icon-text',2,'2017-08-14 10:59:32',NULL),(11,2,'用户管理','/page/perm/userList','prem:user:view',1,'icon-text',3,'2017-08-14 10:59:56',NULL),(12,3,'系统日志','/page/system/logs','sys:log:view',1,'icon-text',1,'2017-08-14 11:00:26',NULL),(13,3,'基本参数','/page/system/basicParameter','sys:basic:view',1,'icon-text',2,'2017-08-14 17:27:18',NULL),(14,3,'友情链接','/page/system/linkList','sys:link:view',1,'icon-text',3,'2017-08-14 17:27:43',NULL),(15,3,'图标管理','/page/system/icons','sys:icon:view',1,'icon-text',4,'2017-08-14 17:28:34',NULL),(16,4,'饼状图','/page/data/pieChart','data:pie:view',1,'icon-text',1,'2017-08-14 17:28:34',NULL),(17,4,'线性图','/page/data/lineChart','data:line:view',1,'icon-text',2,'2017-08-14 17:28:34',NULL),(18,4,'柱状图','/page/data/barChart','data:bar:view',1,'icon-text',3,'2017-08-14 17:28:34',NULL),(19,5,'第三方组件','https://fly.layui.com/extend/','doc:other:view',1,'icon-text',1,'2017-08-14 17:28:34',NULL),(20,6,'增加',NULL,'con:pic:add',2,'icon-text',1,'2017-08-14 17:28:34',NULL),(21,6,'编辑',NULL,'con:pic:edit',2,'icon-text',2,'2017-08-14 17:28:34',NULL),(22,6,'删除',NULL,'con:pic:remove',2,'icon-text',3,'2017-08-14 17:28:34',NULL),(23,6,'批量删除',NULL,'con:pic:batchRemove',2,'icon-text',4,'2017-08-14 17:28:34',NULL),(24,7,'增加',NULL,'con:news:add',2,'icon-text',1,'2017-08-14 17:28:34',NULL),(25,7,'编辑',NULL,'con:news:edit',2,'icon-text',2,'2017-08-14 17:28:34',NULL),(26,7,'刪除',NULL,'con:news:remove',2,'icon-text',3,'2017-08-14 17:28:34',NULL),(27,7,'批量删除',NULL,'con:news:batchRemove',2,'icon-text',4,'2017-08-14 17:28:34',NULL),(28,8,'增加',NULL,'con:file:add',2,'icon-text',1,'2017-08-14 17:28:34',NULL),(29,8,'编辑',NULL,'con:file:edit',2,'icon-text',2,'2017-08-14 17:28:34',NULL),(30,8,'删除',NULL,'con:file:remove',2,'icon-text',3,'2017-08-14 17:28:34',NULL),(31,8,'批量删除',NULL,'con:file:batchRemove',2,'icon-text',4,'2017-08-14 17:28:34',NULL),(32,9,'增加',NULL,'prem:menu:add',2,'icon-text',1,'2017-08-14 17:28:34',NULL),(33,9,'编辑',NULL,'prem:menu:edit',2,'icon-text',2,'2017-08-14 17:28:34',NULL),(34,9,'删除',NULL,'prem:menu:remove',2,'icon-text',3,'2017-08-14 17:28:34',NULL),(35,9,'批量删除',NULL,'prem:menu:batchRemove',2,'icon-text',4,'2017-08-14 17:28:34',NULL),(36,10,'增加',NULL,'prem:role:add',2,'icon-text',1,'2017-08-14 17:28:34',NULL),(37,10,'编辑',NULL,'prem:role:edit',2,'icon-text',2,'2017-08-14 17:28:34',NULL),(38,10,'删除',NULL,'prem:role:remove',2,'icon-text',3,'2017-08-14 17:28:34',NULL),(39,10,'批量删除',NULL,'prem:role:batchRemove',2,'icon-text',4,'2017-08-14 17:28:34',NULL),(40,10,'分配权限',NULL,'prem:role:allot',2,'icon-text',5,'2017-08-14 17:28:34',NULL),(41,11,'增加',NULL,'prem:user:add',2,'icon-text',1,'2017-08-14 17:28:34',NULL),(42,11,'编辑',NULL,'prem:user:edit',2,'icon-text',2,'2017-08-14 17:28:34',NULL),(43,11,'删除',NULL,'prem:user:remove',2,'icon-text',3,'2017-08-14 17:28:34',NULL),(44,11,'批量删除',NULL,'prem:user:batchRemove',2,'icon-text',4,'2017-08-14 17:28:34',NULL),(45,11,'重置密码',NULL,'prem:user:resetPwd',2,'icon-text',5,'2017-08-14 17:28:34',NULL),(46,11,'停用',NULL,'prem:user:stop',2,'icon-text',6,'2017-08-14 17:28:34',NULL),(47,11,'分配角色',NULL,'prem:user:allot',2,'icon-text',7,NULL,NULL);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL COMMENT '角色名称',
  `role_sign` varchar(100) NOT NULL COMMENT '角色标识',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `user_id_create` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`role_name`,`role_sign`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色';

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`role_name`,`role_sign`,`remark`,`user_id_create`,`gmt_create`,`gmt_modified`) values (1,'超级管理员','admin','超级管理员',NULL,'2017-08-12 00:43:52','2017-08-12 19:14:59'),(2,'普通用户','user','普通用户',NULL,'2017-08-12 00:43:52','2017-08-12 00:43:52');

/*Table structure for table `sys_role_menu` */

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `sys_role_menu_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`),
  CONSTRAINT `sys_role_menu_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `sys_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`id`,`role_id`,`menu_id`) values (187,1,1),(188,1,6),(189,1,20),(190,1,21),(191,1,22),(192,1,23),(193,1,7),(194,1,24),(195,1,25),(196,1,26),(197,1,27),(198,1,8),(199,1,28),(200,1,29),(201,1,30),(202,1,31),(203,1,2),(204,1,9),(205,1,32),(206,1,33),(207,1,34),(208,1,35),(209,1,10),(210,1,36),(211,1,37),(212,1,38),(213,1,39),(214,1,40),(215,1,11),(216,1,41),(217,1,42),(218,1,43),(219,1,44),(220,1,45),(221,1,46),(222,1,47),(223,1,3),(224,1,12),(225,1,13),(226,1,14),(227,1,15),(228,1,4),(229,1,16),(230,1,17),(231,1,18),(232,1,5),(233,1,19),(234,2,1),(235,2,6),(236,2,20),(237,2,21),(238,2,22),(239,2,23),(240,2,7),(241,2,24),(242,2,25),(243,2,26),(244,2,27),(245,2,8),(246,2,28),(247,2,29),(248,2,30),(249,2,31),(250,2,3),(251,2,12),(252,2,13),(253,2,14),(254,2,15),(255,2,4),(256,2,16),(257,2,17),(258,2,18),(259,2,5),(260,2,19);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL COMMENT '用户名',
  `nickname` varchar(128) DEFAULT '新用户' COMMENT '昵称',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门id',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(16) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(2) DEFAULT '0' COMMENT '0:正常，1:限制',
  `user_id_create` bigint(20) DEFAULT NULL COMMENT '创建用户id',
  `gmt_create` datetime DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  `sex` bigint(1) DEFAULT NULL COMMENT '0:女，1:男',
  `birth` datetime DEFAULT NULL COMMENT '生日',
  `pic_url` varchar(255) DEFAULT NULL COMMENT '头像链接',
  `live_address` varchar(500) DEFAULT NULL COMMENT '现居住地',
  `hobby` varchar(255) DEFAULT NULL COMMENT '爱好',
  `province` varchar(255) DEFAULT NULL COMMENT '省份',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `district` varchar(255) DEFAULT NULL COMMENT '所在地区',
  PRIMARY KEY (`id`,`username`),
  KEY `dept_id` (`dept_id`),
  CONSTRAINT `sys_user_ibfk_1` FOREIGN KEY (`dept_id`) REFERENCES `sys_dept` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`username`,`nickname`,`password`,`dept_id`,`email`,`phone`,`status`,`user_id_create`,`gmt_create`,`gmt_modified`,`sex`,`birth`,`pic_url`,`live_address`,`hobby`,`province`,`city`,`district`) values (1,'admin','超级管理员','e0b141de1c8091be350d3fc80de66528',6,'izenglong@163.com','15718346460',0,1,'2017-08-15 21:40:39','2017-08-15 21:41:00',96,'2018-04-02 00:00:00','151','创客基地',NULL,'广东省','广州市','番禺区'),(2,'user','普通用户','50780954bc10f69acdf962306f287b32',6,'test@ifast.com','15278792752',0,1,'2017-08-14 13:43:05','2017-08-14 21:15:36',96,'2018-08-22 00:00:00',NULL,NULL,NULL,'北京市','北京市市辖区','东城区'),(3,'张育嘉','新用户','4d6ccab82923814ffc9debd7980e0e50',NULL,NULL,'15718346469',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `sys_user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `sys_user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`id`,`user_id`,`role_id`) values (149,2,2),(150,1,1),(151,1,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
