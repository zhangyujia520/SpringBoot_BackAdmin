package cn.yujiago.springboot.test;

import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import org.json.JSONException;

import java.io.IOException;

public class SendMessage {

    public static void main(String [] args) {

        // 短信应用SDK AppID
        int appid = 1400132234; // 1400开头

        // 短信应用SDK AppKey
        String appkey = "eb50b10be02819ebc9f73477d3de19f1";

        // 需要发送短信的手机号码
        String[] phoneNumbers = {"15718346469"};

        // 短信模板ID，需要在短信应用中申请
        int templateId = 182655; // NOTE: 这里的模板ID`7839`只是一个示例，真实的模板ID需要在短信控制台中申请

        // 签名
        String smsSign = "章鱼的blog"; // NOTE: 这里的签名"腾讯云"只是一个示例，真实的签名需要在短信控制台中申请，另外签名参数使用的是`签名内容`，而不是`签名ID`

        try {
            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);

            String[] params = new String[]{"520241"};

            SmsSingleSenderResult result = ssender.sendWithParam("86", phoneNumbers[0], templateId, params, smsSign, "", "");  // 签名参数未提供或者为空时，会使用默认签名发送短信

            System.out.println(result);
        } catch (HTTPException e) {
            // HTTP响应码错误
            e.printStackTrace();
        } catch (JSONException e) {
            // json解析错误
            e.printStackTrace();
        } catch (IOException e) {
            // 网络IO错误
            e.printStackTrace();
        }

    }

}