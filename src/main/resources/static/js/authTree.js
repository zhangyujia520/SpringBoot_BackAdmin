layui.config({
    base: '../../extends/',
}).extend({
    authtree: 'authtree',
});
layui.use(['jquery', 'authtree', 'form', 'layer'], function(){
    var $ = layui.jquery;
    var authtree = layui.authtree;
    var form = layui.form;
    var layer = layui.layer;

    // 初始化权限树
    // 使用延时，防止取不到隐藏域里的userId
    setTimeout(function () {
        $.ajax({
            // url: '../../json/tree.json',
            url: '/menu/authTreeByRoleId',
            data: {roleId: $(".roleId").val()},
            dataType: 'json',
            success: function (data) {
                // 渲染时传入渲染目标ID，树形结构数据（具体结构看样例，checked表示默认选中），以及input表单的名字
                authtree.render('#LAY-auth-tree-index', data.data, {
                    inputname: 'authids[]',
                    layfilter: 'lay-check-auth',
                    openall: false
                });

                // 监听自定义lay-filter选中状态，PS:layui现在不支持多次监听，所以扩展里边只能改变触发逻辑，然后引起了事件冒泡延迟的BUG，要是谁有好的建议可以反馈我
                form.on('checkbox(lay-check-auth)', function (data) {
                    // 获取所有节点
                    var all = authtree.getAll('#LAY-auth-tree-index');
//                    console.log('all', all);
                    // 获取所有已选中节点
                    var checked = authtree.getChecked('#LAY-auth-tree-index');
//                    console.log('checked', checked);
                    // 获取所有未选中节点
                    var notchecked = authtree.getNotChecked('#LAY-auth-tree-index');
//                    console.log('notchecked', notchecked);
                    // 注意这里：需要等待事件冒泡完成，不然获取叶子节点不准确。
                    setTimeout(function () {
                        // 获取选中的叶子节点
                        var leaf = authtree.getLeaf('#LAY-auth-tree-index');
                        console.log('leaf', leaf);
                    }, 200);
                });
            }
        });
    },80);

    // 监听表单提交
    form.on('submit(LAY-auth-tree-submit)', function(obj){
        // 获取所有已选中节点
        var authids = authtree.getChecked('#LAY-auth-tree-index');
        $.ajax({
            url: '/menu/updateRoleMenu',
            dataType: 'json',
            data: {
                roleId:$(".roleId").val(),
                menuIds:authids
            },
            success: function(res){
                if(res.status == 200){
                    var index=parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                }
            }
        });
        return false;
    });

    // 取消按钮
    $(".cancel_btn").click(function(){
        var index=parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    });
});