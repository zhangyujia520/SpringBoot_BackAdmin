layui.use(['form','layer','table','laytpl','tree'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        table = layui.table;

    // 获取部门树
    $.ajax({
        type: "post",
        url: "/dept/tree",
        datatype: "json",
        contenttype: "application/json; charset=utf-8",
        success: function (treeData) {
            // 部门树
            layui.tree({
                elem: '#departmentTree'
                , nodes: treeData
                , click: function (node) {
                    // 叶子节点执行查询
                    if(node.children == null){
                        table.reload("userListTable",{
                            url: "/user/getUserByDeptId",
                            page: {
                                curr: 1 //重新从第 1 页开始
                            },
                            where: {
                                deptId: node.id
                            }
                        })
                    }
                }
            });
        }
    });

    //用户列表
    var tableIns = table.render({
        elem: '#userList',
        url : '/user/getUserByDeptId',
        cellMinWidth : 65,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 10,
        id : "userListTable",
        cols : [[
            {type: "checkbox", fixed:"left", width:50},
            {field: 'username', title: '用户名', minWidth:100, align:"center"},
            {field: 'nickname', title: '用户昵称', minWidth:100, align:"center"},
            {field: 'email', title: '用户邮箱', minWidth:200, align:'center',templet:function(d){
                return '<a class="layui-blue" href="javascript:;">'+d.email+'</a>';
            }},
            {field: 'sex', title: '用户性别', align:'center',templet:function(d){
                return d.sex == 0 ? '女':'男';
            }},
            {field: 'phone', title: '手机号', align:'center'},
            {title: '操作', minWidth:220, templet:'#userListBar', fixed:"right", align:"center"}
        ]]
    });

    //列表操作
    table.on('tool(userList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;

        if(layEvent === 'edit'){ //编辑
            editUser(data);
        }else if(layEvent === 'allot'){ //分配权限
            allot(data);
        }else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此角色？',{icon:3, title:'提示信息'},function(index){
                $.get("#",{
                    userIds : data.id  //将需要删除的roleId作为参数传入
                },function(data){
                    tableIns.reload();
                    layer.close(index);
                    layer.msg("删除成功");
                })
            });
        }
    });

    // 分配角色
    function allot(edit) {
        var index = layui.layer.open({
            title: "分配角色",
            type: 2,
            content: "/page/perm/allotRole",
            area: ['500px', '500px'],
            success: function (layero, index) {
                var body = layui.layer.getChildFrame('body', index);
                if (edit) {
                    body.find(".userId").val(edit.id);  //id
                    body.find(".username").val(edit.username);  //username
                    form.render();
                }
                setTimeout(function () {
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        })
    }

    // 搜索按钮
    $(".search_btn").on("click",function(){
        if($(".searchVal").val() != ''){
            table.reload("userListTable",{
            	url: "#",
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                    key: $(".searchVal").val()  //搜索的关键字
                }
            })
        }else{
            layer.msg("请输入搜索的内容");
        }
    });

    //添加用户
    function addUser(){
        var index = layui.layer.open({
            title : "添加用户",
            type : 2,
            content : "/page/perm/userAdd",
            area: ['500px', '560px'],
            success : function(layero, index){
                setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        });
        //layui.layer.full(index);
        window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            //layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
    $(".addNews_btn").click(function(){
        addUser();
    });

    //批量删除
    $(".delAll_btn").click(function(){
        var checkStatus = table.checkStatus('userListTable'),
            data = checkStatus.data,
            userIds = [];
        if(data.length > 0) {
            for (var i in data) {
            	userIds += data[i].id+" ";
            }
            layer.confirm('确定删除选中的用户？', {icon: 3, title: '提示信息'}, function (index) {
                $.get("#",{
                    "userIds" : userIds  //将需要删除的userId作为参数传入
                },function(data){
	                tableIns.reload();
	                layer.close(index);
	                layer.msg("删除成功");
                })
            })
        }else{
            layer.msg("请选择需要删除的用户");
        }
    });

    // 导出excel表格
    $(".export_btn").click(function(){
    	window.location.href="/user/excelExport";
    	layer.msg("导出数据");
    });

	// 编辑用户
    function editUser(edit){
        var index = layui.layer.open({
            title : "编辑用户",
            type : 2,
            content : "/page/perm/userEdit",
            area: ['500px', '560px'],
            success : function(layero, index){
                var body = layui.layer.getChildFrame('body', index);
                if(edit){
                	body.find(".id").val(edit.id);  //id
                	body.find(".password").val(edit.password);  //密码
                	body.find(".userEndTime").val(edit.userEndTime);  //登录时间
                    body.find(".userName").val(edit.userName);  //登录名
                    body.find(".userEmail").val(edit.userEmail);  //邮箱
                    body.find(".userSex input[value="+edit.userSex+"]").prop("checked","checked");  //性别
                    body.find(".userGradeHide").val(edit.userGradeId);  //会员等级
                    body.find(".userStatus").val(edit.userStatus);    //用户状态
                    body.find(".userDesc").text(edit.userDesc);    //用户简介
                    form.render();
                }
                setTimeout(function(){
                    layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                },500)
            }
        });
        //layui.layer.full(index);
        //window.sessionStorage.setItem("index",index);
        //改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
        $(window).on("resize",function(){
            //layui.layer.full(window.sessionStorage.getItem("index"));
        })
    }
})
