$(function(){

    $('#tel').keyup(function(event) {
        $('.tel-warn').addClass('hide');
        checkBtn();
    });

    $('#tel').focusout(function(exent) {
        var phone = $.trim($('#tel').val());
        // 如果手机号可注册，则显示用户名框和密码框
        if(checkPhone(phone)){
            $('#uname').removeClass('hide');
            $('#pwd').removeClass('hide');
            $('#confirmpwd').removeClass('hide');
		}else{
            $('#uname').addClass('hide');
            $('#pwd').addClass('hide');
            $('#confirmpwd').addClass('hide');
		}
    });

	$('#veri-code').keyup(function(event) {
		$('.error').addClass('hide');
		checkBtn();
	});

    $('#username').keyup(function(event) {
        $('.uname-err').addClass('hide');
        checkBtn();
    });

    $('#username').focusout(function(exent) {
        var username = $.trim($('#username').val());
        // 检测用户名是否已注册
        checkUsername(username);
    });

    $('#pwd, #confirmpwd').keyup(function(event) {
        $('.pwd-err').addClass('hide');
        $('.confirmpwd-err').addClass('hide');
        checkBtn();
    });

	// 按钮是否可点击
	function checkBtn(){
		var phone = $.trim($('#tel').val());
		var code = $.trim($('#veri-code').val());
		var username = $.trim($('#username').val());
        var pwd = $.trim($('#passport').val());
        var confirmpwd = $.trim($('#passport2').val());
		if (phone != '' && code != '' && username != '' && pwd != '' && confirmpwd != '') {
			$(".lang-btn").removeClass("off");
			sendBtn();
		} else {
			$(".lang-btn").addClass("off").off('click');
		}
	}

	// 检查手机号是否存在
	function checkPhone(phone){
		var status = true;
		if (phone == '') {
			$('.tel-err').removeClass('hide').find("em").text('请输入手机号');
			return false;
		}
		var param = /^1[34578]\d{9}$/;
		if (!param.test(phone)) {
			$('.tel-err').removeClass('hide').text('手机号不合法，请重新输入');
			return false;
		}

		// 检查手机号是否存在
		$.ajax({
            url: '/user/checkPhone',
            type: 'post',
            dataType: 'json',
            async: true,
            data: {phone:phone,type:"register"},
            success:function(data){
                if (data.status == '0') {
                    $('.tel-err').removeClass('hide').text(data.msg);
                    $('.send').removeClass('hide');
                    checkBtn();
                    status = true;
                } else {
                	// 手机号已存在
                    $('.tel-err').removeClass('hide').text(data.msg);
                    $('.send').addClass('hide');
                    $(".lang-btn").addClass('off').off('click');
					status = false;
                }
            },
            error:function(){
				status = false;
            }
        });
		return status;
	}

    // 检查用户名是否存在
    function checkUsername(username){
        var status = true;
        if (username == '') {
            $('.uname-err').removeClass('hide').find("em").text('请输入用户名');
            return false;
        }

        // 检查用户名是否存在
        $.ajax({
            url: '/user/checkUsername',
            type: 'post',
            dataType: 'json',
            async: true,
            data: {username:username},
            success:function(data){
                if (data.status == '0') {
                    $('.uname-err').removeClass('hide').text(data.msg);
                    checkBtn();
                    status = true;
                } else {
                    // 用户名已存在
                    $('.uname-err').removeClass('hide').text(data.msg);
                    $(".lang-btn").addClass('off').off('click');
                    status = false;
                }
            },
            error:function(){
                status = false;
            }
        });
        return status;
    }

	// 检查手机验证码是否输入
	function checkPhoneCode(pCode){
		if (pCode == '') {
			$('.error').removeClass('hide').text('请输入验证码');
			return false;
		} else {
			$('.error').addClass('hide');
			return true;
		}
	}

	// 注册点击事件
	function sendBtn(){
		// 解除之前绑定的事件，防止重复绑定
        $(".lang-btn").off('click');

		$(".lang-btn").click(function(){

			var phone = $.trim($('#tel').val());
			var pcode = $.trim($('#veri-code').val());
            var username = $.trim($('#username').val());
            var pwd = $.trim($('#passport').val());
            var confirmpwd = $.trim($('#passport2').val());
			// 两个密码框不为空且一致
			if(pwd == confirmpwd){

				if (checkPhone(phone) && checkPhoneCode(pcode)) {
					$.ajax({
						url: '/user/pRegister',
						type: 'post',
						dataType: 'json',
						async: true,
						data: {phone:phone,code:pcode,password:pwd,username:username},
						success:function(data){
							if (data.status == '0') {
								setTimeout(function(){
									location.href = "/page/anon/login";
								},1000);
								return true;
							} else if(data.status == '1') {
								$('.tel-err').removeClass('hide').text(data.msg);
								return false;
							} else if(data.status == '2') {
								// 验证码错误
								$('.error').removeClass('hide').text(data.msg);
								return false;
							}
						},
						error:function(){
                            $('.error').removeClass('hide').text('系统错误，请重新注册！');
                            return false;
						}
					});
				} else {
					$('.tel-warn').removeClass('hide').text('注册信息不规范！');
					return false;
				}
            }else{
                $('.pwd-err').removeClass('hide').find("em").text("密码不为空且须相等");
                $('.confirmpwd-err').removeClass('hide').find("em").text("密码不为空且须相等");
                return false;
			}
		});
	}

	// 注册的回车事件
	$(window).keydown(function(event) {
    	if (event.keyCode == 13) {
    		$('.lang-btn').trigger('click');
    	}
  	});

	// 点击发送验证码，获取短信验证码
	$(".form-data").delegate(".send","click",function () {
		var phone = $.trim($('#tel').val());
		if (checkPhone(phone)) {
				$.ajax({
		            url: '/user/sendMessageCode',
		            type: 'post',
		            dataType: 'json',
		            async: true,
		            data: {phone:phone,type:"register"},
		            success:function(data){
		                if (data.status == '0') {
		                    alert(data.msg);
		                } else {
                            alert(data.msg);
		                }
		            },
		            error:function(){
		                
		            }
		    });
	      var oTime = $(".form-data .time"),
				oSend = $(".form-data .send"),
				num = parseInt(oTime.text()),
				oEm = $(".form-data .time em");
		    $(this).hide();
		    oTime.removeClass("hide");
		    var timer = setInterval(function () {
		   		var num2 = num-=1;
	        	oEm.text(num2);
          		if(num2 == 0){
				  clearInterval(timer);
				  oSend.text("重新发送验证码");
				  oSend.show();
				  oEm.text("120");
				  oTime.addClass("hide");
          		}
	      },1000);
		}
  });

});