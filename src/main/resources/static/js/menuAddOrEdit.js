layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

	// 动态生成等级列表
    $.get("/menu/topListAjax",function(data){
    	for(var i = 0; i < data.length; i++){
    		$(".parentMenuList").append('<option value="'+data[i].mid+'">'+data[i].menuName+'</option>');
    	}
    	if($(".parentMidHide").val()){
    		$(".parentMenuList").val($(".parentMidHide").val());
    	}else{
    		$(".parentMenuList").val(data[0].mid);
    	}
    	form.render();
    },"json");
    
    
    form.on("submit(editMenu)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});

         $.post("menuAction_edit.action",{
        	 id : $(".menuId").val(),	//菜单id
        	 mid : data.field.mid,  //mid
             menuName : $(".menuName").val(),  //菜单名
             menuPath : $(".menuPath").val(),	//菜单路径
        	 pareMemuId : $(".parentMenuList").val(),  //父菜单mid
        	 rank : data.field.rank,  //排序号
        	 menuIcon : data.field.menuIcon,  //图标
        	 menuStatus : data.field.status == "on" ? "checked" : ""	//是否启用
         },function(res){
        	 setTimeout(function(){
                 top.layer.close(index);
                 top.layer.msg("菜单修改成功！");
                 layer.closeAll("iframe");
                 //刷新父页面
                 parent.location.reload();
             },1000);
         })
         return false;
    })

    form.on("submit(addMenu)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});

         $.post("menuAction_add.action",{
        	 pareMemuId : $(".parentMenuList").val(),  //一级菜单mid
             menuName : $(".menuName").val(),  //二级菜单名
             menuPath : $(".menuPath").val(),	//菜单路径
             menuStatus : data.field.status == "on" ? "checked":"",    //状态
         },function(res){
        	 setTimeout(function(){
                 top.layer.close(index);
                 top.layer.msg("菜单添加成功！");
                 layer.closeAll("iframe");
                 //刷新父页面
                 parent.location.reload();
             },2000);
         })
         return false;
    })
    
    // 取消按钮
    $(".cancle_btn").click(function(){
    	var index=parent.layer.getFrameIndex(window.name);
    	parent.layer.close(index);
    })
    
})