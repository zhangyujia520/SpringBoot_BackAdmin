layui.use(['form','layer'],function(){
    var form = layui.form
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery;

	// 动态生成等级列表
    $.post("gradeAction_listAjax.action",function(data){

    	$.each(data, function(index, item){
    		$(".userGrade").append(new Option(item.gradeName, item.id));
    	});
    	// 判断隐藏域中是否有等级ID，有则代表编辑，无则代表添加
    	if($(".userGradeHide").val()){
    		$(".userGrade").val($(".userGradeHide").val());
    	}else{
    		$(".userGrade").val(data[0].id);
    	}
    	form.render();
    },"json");
    
    
    form.on("submit(editUser)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});

         $.post("userAction_edit.action",{
        	 id : $(".id").val(),
             userName : $(".userName").val(),  //登录名
             password : $(".password").val(),	//密码
             userEmail : $(".userEmail").val(),  //邮箱
             userSex : data.field.userSex,  //性别
             "grade.id" : data.field.userGrade,  //会员等级
             userStatus : data.field.userStatus,    //用户状态
             userEndTime : $(".userEndTime").val(),    //添加时间
             userDesc : data.field.userDesc    //用户简介
         },function(res){
        	 setTimeout(function(){
                 top.layer.close(index);
                 top.layer.msg("用户修改成功！");
                 layer.closeAll("iframe");
                 //刷新父页面
                 parent.location.reload();
             },2000);
         })
         
        return false;
    })

    form.on("submit(addUser)",function(data){
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});

         $.post("userAction_add.action",{
             userName : $(".userName").val(),  //登录名
             userEmail : $(".userEmail").val(),  //邮箱
             userSex : data.field.userSex,  //性别
             "grade.id" : data.field.userGrade,  //会员等级
             userStatus : data.field.userStatus,    //用户状态
             userEndTime : submitTime,    //添加时间
             userDesc : data.field.userDesc,    //用户简介
         },function(res){
        	 setTimeout(function(){
                 top.layer.close(index);
                 top.layer.msg("用户添加成功！");
                 layer.closeAll("iframe");
                 //刷新父页面
                 parent.location.reload();
             },2000);
         })
         
        return false;
    })
    
    // 取消按钮
    $(".cancle_btn").click(function(){
    	var index=parent.layer.getFrameIndex(window.name);
    	parent.layer.close(index);
    })
    
    //格式化时间
    function filterTime(val){
        if(val < 10){
            return "0" + val;
        }else{
            return val;
        }
    }
    //定时发布
    var time = new Date();
    var submitTime = time.getFullYear()+'-'+filterTime(time.getMonth()+1)+'-'+filterTime(time.getDate())+' '+filterTime(time.getHours())+':'+filterTime(time.getMinutes())+':'+filterTime(time.getSeconds());

})