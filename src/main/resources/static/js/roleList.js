layui.use(['form','layer','table','laytpl','tree'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        table = layui.table;

    //用户列表
    var tableIns = table.render({
        elem: '#roleList',
        url : '/role/list',
        cellMinWidth : 65,
        page : true,
        height : "full-125",
        limits : [10,15,20,25],
        limit : 10,
        id : "roleListTable",
        cols : [[
            {type: "checkbox", fixed:"left", width:50},
            {field: 'roleName', title: '角色名', minWidth:100, align:"center"},
            {field: 'roleSign', title: '角色标识', minWidth:100, align:"center"},
            {field: 'remark', title: '角色备注', minWidth:100, align:"center"},
            {field: 'gmtCreate', title: '创建时间', minWidth:100, align:"center"},
            {field: 'gmtModified', title: '修改时间', minWidth:100, align:"center"},
            {title: '操作', minWidth:175, templet:'#roleListBar', fixed:"right", align:"center"}
        ]]
    });

    //列表操作
    table.on('tool(roleList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;

        if(layEvent === 'edit'){ //编辑
            editRole(data);
        }else if(layEvent === 'allot'){ //分配权限
            allot(data);
        }else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此角色？',{icon:3, title:'提示信息'},function(index){
                $.get("#",{
                    userIds : data.id  //将需要删除的roleId作为参数传入
                },function(data){
                    tableIns.reload();
                    layer.close(index);
                    layer.msg("删除成功");
                })
            });
        }
    });
    //编辑用户
    function allot(edit) {
        var index = layui.layer.open({
            title: "分配权限",
            type: 2,
            content: "/page/perm/authTree",
            area: ['500px', '560px'],
            success: function (layero, index) {
                var body = layui.layer.getChildFrame('body', index);
                if (edit) {
                    body.find(".roleId").val(edit.id);  //id
                    body.find(".roleName").val(edit.roleName);  //roleName
                    form.render();
                }
                setTimeout(function () {
                    layui.layer.tips('点击此处返回角色列表', '.layui-layer-setwin .layui-layer-close', {
                        tips: 3
                    });
                }, 500)
            }
        })
    }
})