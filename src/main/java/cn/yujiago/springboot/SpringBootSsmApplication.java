package cn.yujiago.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages = "cn.yujiago.springboot.mapper") // mapper接口的路径
@EnableCaching	// 开启缓存注解
@EnableTransactionManagement// 开启事务注解
public class SpringBootSsmApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSsmApplication.class, args);
	}
}
