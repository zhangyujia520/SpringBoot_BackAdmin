package cn.yujiago.springboot.utils;

import cn.yujiago.springboot.config.Constants;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import org.json.JSONException;

import java.io.IOException;

public class MessageUtil {

    // 短信应用SDK AppID
    private static final int appid = Constants.APPID; // 1400开头

    // 短信应用SDK AppKey
    private static final String appkey = Constants.APPKEY;

    // 需要发送短信的手机号码
    // String[] phoneNumbers = {"15718346466", "15718346467"};

    // 短信模板ID，需要在短信应用中申请
    private static final int templateId = Constants.TEMPLATE_ID; // NOTE: 这里的模板ID`7839`只是一个示例，真实的模板ID需要在短信控制台中申请

    // 签名
    private static final String smsSign = Constants.SMS_SIGN; // NOTE: 这里的签名"腾讯云"只是一个示例，真实的签名需要在短信控制台中申请，另外签名参数使用的是`签名内容`，而不是`签名ID`

    /**
     * 发送短信验证码
     * @param phoneNumbers
     * @param params
     */
    public static void sendMessage(String[] phoneNumbers, String[] params){
        try {
            SmsSingleSender ssender = new SmsSingleSender(appid, appkey);

            SmsSingleSenderResult result = ssender.sendWithParam("86", phoneNumbers[0], templateId, params, smsSign, "", "");  // 签名参数未提供或者为空时，会使用默认签名发送短信

            // System.out.println(result);
        } catch (HTTPException e) {
            // HTTP响应码错误
            e.printStackTrace();
        } catch (JSONException e) {
            // json解析错误
            e.printStackTrace();
        } catch (IOException e) {
            // 网络IO错误
            e.printStackTrace();
        }
    }

}
