package cn.yujiago.springboot.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Random;
import java.util.UUID;

public class MyStringUtils {
    /**
     * 获得md5加密后的数据
     * @param value 明文
     * @return 密文
     */
    public static String getMD5Value(String value){

        try {
            //1 获得jdk提供消息摘要算法工具类
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            //2 加密的结果10进制
            byte[] md5ValueByteArray = messageDigest.digest(value.getBytes());
            //3将10进制 转换16进制
            BigInteger bigInteger = new BigInteger(1 , md5ValueByteArray);

            return bigInteger.toString(16);
        } catch (Exception e) {
            throw new RuntimeException(e);
            //如果出现了异常，将默认值
            //return value;
        }
    }

    /**
     * 返回32位字符串随机数
     * @return
     */
    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 返回文件扩展（包括 .）
     * @param fileName
     * @return
     */
    public static String getFileSuffix(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."));
    }

    public static final Random random = new Random();

    /**
     * 随机生成6位数字字符串
     * @return
     */
    public static String getNumberCode(){
        return String.valueOf(random.nextInt(999999-100000)+100000);
    }
}
