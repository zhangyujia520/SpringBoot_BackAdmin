package cn.yujiago.springboot.service.impl;

import cn.yujiago.springboot.config.Constants;
import cn.yujiago.springboot.mapper.ImageMapper;
import cn.yujiago.springboot.pojo.BackAdminResult;
import cn.yujiago.springboot.entity.Image;
import cn.yujiago.springboot.pojo.PictureResult;
import cn.yujiago.springboot.service.ImageService;
import cn.yujiago.springboot.utils.FastDFSClient;
import cn.yujiago.springboot.utils.MyStringUtils;
import cn.yujiago.springboot.utils.QiniuCloudUtil;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@CacheConfig(cacheNames = "image", cacheManager = "redisCacheManager")
@Service
public class ImageServiceImpl implements ImageService {

    @Resource
    private ImageMapper imageMapper;

    @Override
    @Cacheable(cacheNames = "image", key = "'getAllImage'", unless = "#result==null")
    public List<Image> getAllImage() {
        List<Image> images = imageMapper.selectAllImage();
        return images;
    }

    @Override
    public PictureResult uploadImageToFastDFS(MultipartFile picFile) {

        // 判断图片是否为空
        if(picFile.isEmpty()) {
            return PictureResult.builder().code(404).msg("图片为空！").build();
        }
        // 上传到图片服务器
        try {
            // 取图片扩展名
            String originalFilename = picFile.getOriginalFilename();
            String extName = originalFilename.substring(originalFilename.lastIndexOf(".")+1);

            FastDFSClient client = new FastDFSClient("classpath:properties/client.conf");
            String url = client.uploadFile(picFile.getBytes(), extName);
            // 拼接图片服务器完整图片地址
            url = Constants.IMAGE_SERVER_BASE_URL + url;

            return PictureResult.builder().code(200).msg("图片上传成功！").url(url).build();
        } catch (Exception e) {
            e.printStackTrace();
            return PictureResult.builder().code(500).msg("图片上传失败！").build();
        }
    }

    @Override
    public PictureResult uploadImageToQiniu(MultipartFile picFile) {

        // 判断图片是否为空
        if(picFile.isEmpty()) {
            return PictureResult.builder().code(404).msg("图片为空！").build();
        }
        try {
            byte[] bytes = picFile.getBytes();
            // 获取文件扩展名
            String suffix = MyStringUtils.getFileSuffix(picFile.getOriginalFilename());
            String imageName = UUID.randomUUID().toString() + suffix;

            //使用base64方式上传到七牛云
            String url = QiniuCloudUtil.put64image(bytes, imageName);

            return PictureResult.builder().code(200).msg("文件上传成功！").url(url).build();
        } catch (Exception e) {
            return PictureResult.builder().code(500).msg("文件上传发生异常！").build();
        }
    }

    @Override
    @CacheEvict(cacheNames = "image", key = "'getAllImage'")
    public void saveImageInfo(String url) {

        Image image = Image.builder().id(MyStringUtils.getUUID()).url(url).isShow(0).title("QINIU").build();

        imageMapper.insertImage(image);
    }

    @Override
    @Transactional
    @CacheEvict(cacheNames = "image", key = "'getAllImage'")
    public BackAdminResult deleteImageByIds(String[] ids) {

        List<String> strings = Arrays.asList(ids);

        imageMapper.updateImageIsShow(strings);

        return BackAdminResult.build(200, "删除成功");
    }
}
