package cn.yujiago.springboot.service.impl;

import cn.yujiago.springboot.component.ReportExcel;
import cn.yujiago.springboot.entity.UserDO;
import cn.yujiago.springboot.mapper.UserMapper;
import cn.yujiago.springboot.pojo.BackAdminResult;
import cn.yujiago.springboot.pojo.LayuiTableResult;
import cn.yujiago.springboot.service.UserService;
import cn.yujiago.springboot.utils.MessageUtil;
import cn.yujiago.springboot.utils.MyStringUtils;
import cn.yujiago.springboot.utils.ShiroUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private PageHelper pageHelper;
    @Resource
    private ReportExcel reportExcel;

    @Override
    public UserDO getUser(String username, String password) {

        Example example = new Example(UserDO.class);
        Example.Criteria criteria = example.createCriteria();

        criteria.andEqualTo("username", username);
        criteria.andEqualTo("password", password);

        return userMapper.selectOneByExample(example);
    }

    @Override
    public UserDO getUser(String username) {
        Example example = new Example(UserDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("username", username);

        List<UserDO> userDOS = userMapper.selectByExample(example);
        if(userDOS != null && userDOS .size() > 0){
            return userDOS.get(0);
        }else{
            return null;
        }
    }

    @Override
    public LayuiTableResult<UserDO> getUserByDeptId(Long deptId, int page, int limit) {

        // 1、设置分页信息，包括当前页数和每页显示的总计数
        pageHelper.startPage(page, limit);

        List<UserDO> userDOS = null;
        if(deptId != null && deptId != 0){
            Example example = new Example(UserDO.class);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("deptId", deptId);

            userDOS = userMapper.selectByExample(example);
        }
        else{
            userDOS = userMapper.selectAll();
        }

        // 3、获取分页查询后的数据
        PageInfo<UserDO> pageInfo = new PageInfo<>(userDOS);

        LayuiTableResult<UserDO> result = new LayuiTableResult<>();
        result.setCode(0);
        result.setMsg("查询成功！");
        result.setData(userDOS);
        result.setCount(pageInfo.getSize());

        return result;
    }

    @Override
    public boolean checkPhoneExists(String phone) {

        Example example = new Example(UserDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("phone", phone);

        long count = userMapper.selectCountByExample(example);

        if(count > 0){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean sendMessageCode(String phone, HttpSession session) {

        // 6位验证码
        String code = MyStringUtils.getNumberCode();

        // 调用第三方API发送短信验证码
        MessageUtil.sendMessage(new String[]{phone}, new String[]{code});

        // 将手机号和验证码以key-value的形式存入session
        session.setAttribute(phone, code);

        return true;
    }

    @Transactional
    @Override
    public BackAdminResult register(UserDO user, String code, HttpSession session) {

        // 根据phone从session中取出发送的短信验证码，并与用户输入的验证码比较
        String messageCode = (String) session.getAttribute(user.getPhone());

        if(StringUtils.isNoneBlank(messageCode) && messageCode.equals(code)){
            // 加密密码
            String hashPassword = ShiroUtils.getSimpleHash(user.getPassword(), user.getUsername()).toString();
            user.setPassword(hashPassword);

            // 将注册信息插入数据库
            userMapper.insert(user);

            return BackAdminResult.build(0, "注册成功！");
        }else{
            return BackAdminResult.build(2, "验证码错误！");
        }
    }

    @Override
    public UserDO selectByPhone(String phone) {

        Example example = new Example(UserDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("phone", phone);

        List<UserDO> userDOS = userMapper.selectByExample(example);

        if(userDOS != null && userDOS.size() > 0){
            return userDOS.get(0);
        }else{
            return null;
        }
    }

    @Override
    public void excelExport(HttpServletRequest request, HttpServletResponse response) {

        List<UserDO> userList = userMapper.selectAll();

        try {
            reportExcel.excelExport(userList,"用户数据", UserDO.class,1, response, request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
