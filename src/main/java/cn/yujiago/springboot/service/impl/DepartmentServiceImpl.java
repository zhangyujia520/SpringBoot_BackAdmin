package cn.yujiago.springboot.service.impl;

import cn.yujiago.springboot.entity.DeptDO;
import cn.yujiago.springboot.mapper.DepartmentMapper;
import cn.yujiago.springboot.pojo.LayuiTreeNode;
import cn.yujiago.springboot.service.DepartmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Resource
    private DepartmentMapper departmentMapper;

    @Override
    public List<LayuiTreeNode<DeptDO>> getDepartmentTree() {
        // 查询所有部门
        List<DeptDO> deptDOS = departmentMapper.selectAll();
        // 将部门转换成树节点所需格式
        List<LayuiTreeNode<DeptDO>> rootList = new ArrayList<>();
        // 生成树结构
        List<LayuiTreeNode<DeptDO>> nodeList = new ArrayList<>();

        deptDOS.forEach(deptDO -> {
            LayuiTreeNode<DeptDO> node = new LayuiTreeNode<>();
            node.setId(deptDO.getId());
            node.setParentId(deptDO.getParentId());
            node.setName(deptDO.getName());
            rootList.add(node);
        });

        // 找出父节点
        rootList.forEach(node -> {
            if(node.getParentId() != null && node.getParentId() == 0){
                nodeList.add(node);
            }
        });

        // 为父节点添加子菜单，递归调用
        for(LayuiTreeNode node : nodeList){
            node.setChildren(getChild(node.getId(), rootList));
        }

        return nodeList;
    }

    private List<LayuiTreeNode> getChild(Long id, List<LayuiTreeNode<DeptDO>> rootList){
        // 子菜单
        List<LayuiTreeNode> childList = new ArrayList<>();

        rootList.forEach(node -> {
            // 遍历所有节点，将父菜单id与传过来的id比较
            if (node.getParentId() != null && node.getParentId() != 0) {
                if (node.getParentId().equals(id)) {
                    childList.add(node);
                }
            }
        });

        // 把子菜单的子菜单再循环一遍
        childList.forEach(node -> {
            if (node.getParentId() != null && node.getParentId() != 0) {
                // 递归
                node.setChildren(getChild(node.getId(), rootList));
            }
        });
        // 递归退出条件
        if (childList.size() == 0) {
            return null;
        }
        return childList;
    }
}
