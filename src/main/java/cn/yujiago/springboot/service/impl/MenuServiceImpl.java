package cn.yujiago.springboot.service.impl;

import cn.yujiago.springboot.entity.MenuDO;
import cn.yujiago.springboot.entity.RoleDO;
import cn.yujiago.springboot.mapper.MenuMapper;
import cn.yujiago.springboot.mapper.RoleMapper;
import cn.yujiago.springboot.pojo.AuthNode;
import cn.yujiago.springboot.pojo.LayuiTableResult;
import cn.yujiago.springboot.pojo.MenuNode;
import cn.yujiago.springboot.pojo.ResponseEntity;
import cn.yujiago.springboot.service.MenuService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MenuServiceImpl implements MenuService {

    @Resource
    private MenuMapper menuMapper;
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private PageHelper pageHelper;

    @Override
    public Set<String> getPermsByUserId(Long userId) {

        Set<String> permSet = new HashSet<>();

        // 根据用户ID查询角色，再根据角色ID查询相应的权限
        List<RoleDO> roles = roleMapper.selectRoles(userId);

        for(RoleDO role : roles){
            List<String> permList = menuMapper.selectPerms(role.getId());
            for(String perm : permList){
                permSet.add(perm);
            }
        }
        return permSet;
    }

    @Override
    public LayuiTableResult<AuthNode> getAuthTreeByUserId(Long userId) {

        // 查出所有菜单
        List<MenuDO> menuDOS = menuMapper.selectAll();

        // 查出用户所拥有权限
        Set<String> perms = this.getPermsByUserId(userId);
        // 转换成权限树的初始链表
        List<AuthNode> rootList = new ArrayList<>();
        // 遍历后拥有树结构的链表
        List<AuthNode> nodeList = new ArrayList<>();

        for (MenuDO menu : menuDOS){
            if(perms.contains(menu.getPerms())){
                rootList.add(new AuthNode(menu.getId(), menu.getParentId(), menu.getName(), true));
            }else{
                rootList.add(new AuthNode(menu.getId(), menu.getParentId(), menu.getName(), false));
            }
        }
        // 找出父节点
        for (AuthNode node : rootList){
            if(node.getParentId() != null && node.getParentId() == 0){
                nodeList.add(node);
            }
        }
        // 为父节点添加子菜单，递归调用
        for(AuthNode node : nodeList){
            node.setList(getAuthChild(node.getValue(), rootList));
        }

        LayuiTableResult<AuthNode> result = new LayuiTableResult<>();
        result.setCode(0);
        result.setMsg("查询成功！");
        result.setData(nodeList);

        return result;
    }

    @Override
    public LayuiTableResult<AuthNode> getAuthTreeByRoleId(Long roleId) {

        // 查出所有菜单
        List<MenuDO> menuDOS = menuMapper.selectAll();

        // 查出角色所拥有的权限
        List<String> perms = menuMapper.selectPerms(roleId);
        // 转换成权限树的初始链表
        List<AuthNode> rootList = new ArrayList<>();
        // 遍历后拥有树结构的链表
        List<AuthNode> nodeList = new ArrayList<>();

        for (MenuDO menu : menuDOS){
            if(perms.contains(menu.getPerms())){
                rootList.add(new AuthNode(menu.getId(), menu.getParentId(), menu.getName(), true));
            }else{
                rootList.add(new AuthNode(menu.getId(), menu.getParentId(), menu.getName(), false));
            }
        }
        // 找出父节点
        for (AuthNode node : rootList){
            if(node.getParentId() != null && node.getParentId() == 0){
                nodeList.add(node);
            }
        }
        // 为父节点添加子菜单，递归调用
        for(AuthNode node : nodeList){
            node.setList(getAuthChild(node.getValue(), rootList));
        }

        LayuiTableResult<AuthNode> result = new LayuiTableResult<>();
        result.setCode(0);
        result.setMsg("查询成功！");
        result.setData(nodeList);

        return result;
    }

    @Override
    public LayuiTableResult<MenuDO> getAllMenu(){

        List<MenuDO> menuDOS = menuMapper.selectAll();

        LayuiTableResult<MenuDO> result = new LayuiTableResult<>();
        result.setCode(0);
        result.setMsg("查询成功！");
        result.setData(menuDOS);
        result.setCount(menuDOS.size());

        return result;
    }

    @Override
    public LayuiTableResult<MenuDO> getMenusByCondition(Long page, Long limit) {
        // 1、设置分页信息，包括当前页数和每页显示的总计数
        pageHelper.startPage(page.intValue(), limit.intValue());

        List<MenuDO> menuDOS = menuMapper.selectAll();

        PageInfo<MenuDO> pageInfo = new PageInfo<>(menuDOS);

        LayuiTableResult<MenuDO> result = new LayuiTableResult<>();
        result.setCode(0);
        result.setMsg("查询成功！");
        result.setData(menuDOS);
        result.setCount(pageInfo.getTotal());

        return result;
    }

    @Override
    public ResponseEntity updateRoleMenu(Long roleId, List<Long> menuIds) {

        // 删除该角色的权限
        menuMapper.deleteRoleMenuByRoleId(roleId);
        if(menuIds != null){
            // 插入该角色的最新所有权限
            menuMapper.insertRoleMenu(roleId, menuIds);
        }
        return ResponseEntity.build(200, "执行成功！");
    }

    @Override
    public List<MenuNode> getMenuTree() {

        Example example = new Example(MenuDO.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLessThanOrEqualTo("type", 1);

        // 查出一二级菜单
        List<MenuDO> menuDOS = menuMapper.selectByExample(example);
        // 初始MenuNode链表
        List<MenuNode> rootList = new ArrayList<>();
        // 具有树结构的链表
        List<MenuNode> resultList = new ArrayList<>();

        menuDOS.forEach(menu -> {
            MenuNode node = new MenuNode();
            node.setTitle(menu.getName());
            node.setIcon(menu.getIcon());
            node.setHref(menu.getUrl());
            node.setParentId(menu.getParentId());
            node.setId(menu.getId());
            rootList.add(node);
        });

        // 找出父节点
        for (MenuNode node : rootList){
            if(node.getParentId() != null && node.getParentId() == 0){
                resultList.add(node);
            }
        }
        // 为父节点添加子菜单，递归调用
        for(MenuNode node : resultList){
            node.setChildren(getMenuChild(node.getId(), rootList));
        }

        return resultList;
    }

    private List<AuthNode> getAuthChild(Long id, List<AuthNode> rootList){
        // 子菜单
        List<AuthNode> childList = new ArrayList<>();
        for (AuthNode node : rootList) {
            // 遍历所有节点，将父菜单id与传过来的id比较
            if (node.getParentId() != null && node.getParentId() != 0) {
                if (node.getParentId().equals(id)) {
                    childList.add(node);
                }
            }
        }
        // 把子菜单的子菜单再循环一遍
        for (AuthNode node : childList) {// 没有url子菜单还有子菜单
            if (node.getParentId() != null && node.getParentId() != 0) {
                // 递归
                node.setList(getAuthChild(node.getValue(), rootList));
            }
        }
        // 递归退出条件
        if (childList.size() == 0) {
            return null;
        }
        return childList;
    }

    private List<MenuNode> getMenuChild(Long id, List<MenuNode> rootList){
        // 子菜单
        List<MenuNode> childList = new ArrayList<>();
        for (MenuNode node : rootList) {
            // 遍历所有节点，将父菜单id与传过来的id比较
            if (node.getParentId() != null && node.getParentId() != 0) {
                if (node.getParentId().equals(id)) {
                    childList.add(node);
                }
            }
        }
        // 把子菜单的子菜单再循环一遍
        for (MenuNode node : childList) {// 没有url子菜单还有子菜单
            if (node.getParentId() != null && node.getParentId() != 0) {
                // 递归
                node.setChildren(getMenuChild(node.getId(), rootList));
            }
        }
        // 递归退出条件
        if (childList.size() == 0) {
            return null;
        }
        return childList;
    }
}
