package cn.yujiago.springboot.service;

import cn.yujiago.springboot.entity.File;
import cn.yujiago.springboot.pojo.FileResult;
import cn.yujiago.springboot.pojo.LayuiTableResult;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    /**
     * 上传文件到七牛云，并保存到数据库
     * @param file
     * @return
     */
    FileResult uploadFileToQiniuAndInsert(MultipartFile file);

    /**
     * 根据条件查询，并分页
     * @param page
     * @param limit
     * @param key
     * @return
     */
    LayuiTableResult<File> getFileByCondition(Integer page, Integer limit, String key);
}
