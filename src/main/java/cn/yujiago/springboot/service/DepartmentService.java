package cn.yujiago.springboot.service;


import cn.yujiago.springboot.entity.DeptDO;
import cn.yujiago.springboot.pojo.LayuiTreeNode;

import java.util.List;

public interface DepartmentService {

    /**
     * 获取部门树
     * @return
     */
    List<LayuiTreeNode<DeptDO>> getDepartmentTree();

}
