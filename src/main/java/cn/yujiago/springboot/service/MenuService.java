package cn.yujiago.springboot.service;


import cn.yujiago.springboot.entity.MenuDO;
import cn.yujiago.springboot.pojo.AuthNode;
import cn.yujiago.springboot.pojo.LayuiTableResult;
import cn.yujiago.springboot.pojo.MenuNode;
import cn.yujiago.springboot.pojo.ResponseEntity;

import java.util.List;
import java.util.Set;

public interface MenuService {

    /**
     * 根据用户ID查询其所有权限
     * @param userId
     * @return
     */
    Set<String> getPermsByUserId(Long userId);

    /**
     * 根据用户ID，获取权限树
     * @param userId
     * @return
     */
    LayuiTableResult<AuthNode> getAuthTreeByUserId(Long userId);

    /**
     * 根据角色ID，获取权限树
     * @param roleId
     * @return
     */
    LayuiTableResult<AuthNode> getAuthTreeByRoleId(Long roleId);

    /**
     * 查询所有菜单
     * @return
     */
    public LayuiTableResult<MenuDO> getAllMenu();

    /**
     * 分页查询菜单
     * @param page
     * @param limit
     * @return
     */
    LayuiTableResult<MenuDO> getMenusByCondition(Long page, Long limit);

    /**
     * 更新该角色的权限
     * @param roleId
     * @param menuIds
     * @return
     */
    ResponseEntity updateRoleMenu(Long roleId, List<Long> menuIds);

    /**
     * 动态生成菜单
     * @return
     */
    List<MenuNode> getMenuTree();
}
