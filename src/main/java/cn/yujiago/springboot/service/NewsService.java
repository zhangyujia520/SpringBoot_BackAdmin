package cn.yujiago.springboot.service;

import cn.yujiago.springboot.pojo.LayuiTableResult;
import cn.yujiago.springboot.entity.News;

public interface NewsService {

    /**
     * 按条件查询文章
     * @param page
     * @param rows
     * @param newsName
     * @return
     */
    LayuiTableResult<News> getNewsByCondition(Integer page, Integer rows, String newsName);

}
