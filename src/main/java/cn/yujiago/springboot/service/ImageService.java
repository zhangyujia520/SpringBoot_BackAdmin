package cn.yujiago.springboot.service;

import cn.yujiago.springboot.pojo.BackAdminResult;
import cn.yujiago.springboot.entity.Image;
import cn.yujiago.springboot.pojo.PictureResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageService {

    /**
     * 查询所有图片
     * @return
     */
    List<Image> getAllImage();

    /**
     * 上传图片到FastDFS
     */
    PictureResult uploadImageToFastDFS(MultipartFile picFile);

    /**
     * 上传图片到七牛云
     * @param picFile
     * @return
     */
    PictureResult uploadImageToQiniu(MultipartFile picFile);

    /**
     * 将图片信息保存到数据库
     * @param url
     */
    void saveImageInfo(String url);

    /**
     * 批量删除图片
     * @param ids
     * @return
     */
    BackAdminResult deleteImageByIds(String[] ids);

}
