package cn.yujiago.springboot.service;

import cn.yujiago.springboot.entity.UserDO;
import cn.yujiago.springboot.pojo.BackAdminResult;
import cn.yujiago.springboot.pojo.LayuiTableResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public interface UserService {

    /**
     * 根据用户名和密码查找
     */
    UserDO getUser(String username, String password);

    /**
     * 根据用户名查找
     */
    UserDO getUser(String username);

    /**
     * 根据部门Id查找员工
     */
    LayuiTableResult<UserDO> getUserByDeptId(Long deptId, int page, int limit);

    /**
     * 检测手机号是否存在
     */
    boolean checkPhoneExists(String phone);

    /**
     * 发送短信验证码
     */
    boolean sendMessageCode(String phone, HttpSession session);

    /**
     * 注册
     */
    BackAdminResult register(UserDO user, String code, HttpSession session);

    /**
     * 通过手机号查找
     */
    UserDO selectByPhone(String phone);

    /**
     * 导出excel
     */
    void excelExport(HttpServletRequest request, HttpServletResponse response);

}
