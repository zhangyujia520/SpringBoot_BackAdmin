package cn.yujiago.springboot.service;


import cn.yujiago.springboot.entity.RoleDO;
import cn.yujiago.springboot.pojo.LayuiTableResult;
import cn.yujiago.springboot.pojo.ResponseEntity;
import cn.yujiago.springboot.pojo.RoleNode;

import java.util.List;

public interface RoleService {

    /**
     * 根据用户ID查询角色
     * @param userId
     * @return
     */
    List<RoleDO> getRoles(Long userId);

    /**
     * 分页查询角色
     * @param page
     * @param limit
     * @return
     */
    LayuiTableResult<RoleDO> getRolesByCondition(int page, int limit);

    /**
     * 查出所有角色，并标记指定用户所拥有的角色
     * @param userId
     * @return
     */
    List<RoleNode> getRolesByUserId(Long userId);

    /**
     * 根据用户ID更新用户所拥有的角色
     * @param userId
     * @param roleIds
     * @return
     */
    ResponseEntity updateUserRole(Long userId, List<Long> roleIds);
}
