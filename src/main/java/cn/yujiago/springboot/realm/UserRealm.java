package cn.yujiago.springboot.realm;


import cn.yujiago.springboot.entity.RoleDO;
import cn.yujiago.springboot.entity.UserDO;
import cn.yujiago.springboot.exception.CustomAuthenticationException;
import cn.yujiago.springboot.service.MenuService;
import cn.yujiago.springboot.service.RoleService;
import cn.yujiago.springboot.service.UserService;
import cn.yujiago.springboot.utils.ShiroUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserRealm extends AuthorizingRealm {

    @Autowired
    private MenuService menuService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    /**
     * 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {

        System.out.println("--------开始授权--------");

        //获取用户
        UserDO user = (UserDO) SecurityUtils.getSubject().getPrincipal();

        // 角色
        List<RoleDO> roles = roleService.getRoles(user.getId());
        Set<String> roleSet = new HashSet<>();
        for(RoleDO role : roles){
            roleSet.add(role.getRoleSign());
        }
        // 权限
        Set<String> permsSet = menuService.getPermsByUserId(user.getId());

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        // 注意权限Set中不能含有null，要么在set中处理，要么在sql中处理
        roleSet.remove(null);
        permsSet.remove(null);
        info.addRoles(roleSet);
        info.setStringPermissions(permsSet);
        return info;
    }

    /**
     * 认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        UsernamePasswordToken token = null;

        if(authenticationToken instanceof UsernamePasswordToken){
            token = (UsernamePasswordToken) authenticationToken;
        }else{
            return null;
        }

        String username = token.getUsername();

        if(StringUtils.isBlank(username)){
            return null;
        }

        UserDO user = userService.getUser(token.getUsername());

        // 账号不存在
        if (user == null) {
            throw new CustomAuthenticationException("账号或密码不正确");
        }

        // 密码错误，如果数据库的密码已加密，这里需要把登录时输入的密码加密后再比对
//        if (!password.equals(user.getPassword())) {
//            throw new IncorrectCredentialsException("账号或密码不正确");
//        }

        // 账号锁定
        if (user.getStatus() == 1) {
            throw new CustomAuthenticationException("账号已被锁定,请联系管理员");
        }

        SimpleAuthenticationInfo info = ShiroUtils.getSimpleAuthenticationInfo(user, user.getPassword(), token.getUsername(), this.getName());

        return info;
    }

    @Override
    public boolean supports(AuthenticationToken var1){
        return var1 instanceof UsernamePasswordToken;
    }
}
