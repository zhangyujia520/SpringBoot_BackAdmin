package cn.yujiago.springboot.interceptor;


import cn.yujiago.springboot.config.Constants;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 登录拦截器
 */
//@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

            HttpSession session = request.getSession();

            // 从session中检查是否存在已登录的用户信息
            if(session.getAttribute(Constants.LOGIN_ADMIN_KEY) == null){
                response.sendRedirect("/page/anon/login");
                return false;
            }else{
                return true;
            }
    }
}
