package cn.yujiago.springboot.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 菜单实体类
 */
@Setter
@Getter
@Table(name = "menu")
public class Menu implements Serializable {

    @Id
    private String id;
    private Integer mid;
    private String menuName;
    private String menuPath;
    private Integer pareMemuId;
    private Integer rank;
    private String menuStatus;
    private String menuIcon;

}
