package cn.yujiago.springboot.entity;

import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "news")
public class News implements Serializable {

    @Id
    private String id;
    private String newsName;
    private String newsAbstract;
    private String newsContent;
    private String classify;
    private String newsStatus;
    private String newsTime;
    private String newsLook;
    private String newsTop;

    private UserDO user;
}
