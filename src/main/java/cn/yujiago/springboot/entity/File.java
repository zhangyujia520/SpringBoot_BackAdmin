package cn.yujiago.springboot.entity;

import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "file")
public class File implements Serializable {

    @Id
    private String id;
    private String fileName;
    private String url;
    private Long size;
    private Timestamp uploadTime;
    private Integer status;

}
