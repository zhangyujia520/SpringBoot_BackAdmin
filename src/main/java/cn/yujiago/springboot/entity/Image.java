package cn.yujiago.springboot.entity;

import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "image")
public class Image implements Serializable {

    @Id
    private String id;
    private String title;
    private String remark;
    private String url;
    private Integer isShow;

}