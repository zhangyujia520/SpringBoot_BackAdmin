package cn.yujiago.springboot.entity;

import cn.yujiago.springboot.annotation.ExcelAnnotation;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Table(name = "sys_user")
public class UserDO implements Serializable {
    @Id
    private Long id;
    // 用户名
    @ExcelAnnotation(id=1,name={"用户名"},width = 5000)
    private String username;
    // 昵称
    @ExcelAnnotation(id=2,name={"昵称"},width = 5000)
    private String nickname;
    // 密码
    private String password;
    // 部门
    @ExcelAnnotation(id=3,name={"部门"},width = 5000)
    private Long deptId;
    // 邮箱
    @ExcelAnnotation(id=4,name={"邮箱"},width = 5000)
    private String email;
    // 手机号
    @ExcelAnnotation(id=5,name={"手机号"},width = 5000)
    private String phone;
    // 状态 0:正常，1:禁用
    @ExcelAnnotation(id=6,name={"状态"},width = 5000)
    private Short status;
    // 创建用户id
    private Long userIdCreate;
    // 创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;
    // 修改时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;
    // 性别
    @ExcelAnnotation(id=7,name={"性别"},width = 5000)
    private Short sex;
    // 出身日期
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd")
    @ExcelAnnotation(id=8,name={"出身日期"},width = 5000)
    private Date birth;
    // 图片链接
    private String picUrl;
    // 现居住地
    private String liveAddress;
    // 爱好
    private String hobby;
    // 省份
    private String province;
    // 所在城市
    private String city;
    // 所在地区
    private String district;

}
