package cn.yujiago.springboot.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Table(name = "sys_dept")
public class DeptDO implements Serializable {
    @Id
    private Long id;
    // 上级部门ID，一级部门为0
    private Long parentId;
    // 部门名称
    private String name;
    // 排序
    private Integer orderNum;
    // 是否删除  0：正常，1：已删除
    private Integer delFlag;

}
