package cn.yujiago.springboot.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Layui树所需数据格式
 * @param <T>
 */
public class LayuiTreeNode<T> implements Serializable {

    private Long id;
    private Long parentId;
    private String name;
    private List<T> children;

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<T> getChildren() {
        return children;
    }

    public void setChildren(List<T> children) {
        this.children = children;
    }
}
