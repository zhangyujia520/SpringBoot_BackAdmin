package cn.yujiago.springboot.pojo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 用于展示权限树，并显示权限节点是否被当前用户或角色所拥有
 */
@Getter
@Setter
public class AuthNode implements Serializable {

    private Long value;
    private String name;
    private Long parentId;
    // 是否被当前用户所拥有
    private boolean checked;

    private List<AuthNode> list;

    public AuthNode(Long value, Long parentId, String name, boolean checked){
        this.value = value;
        this.parentId = parentId;
        this.name = name;
        this.checked = checked;
    }

}
