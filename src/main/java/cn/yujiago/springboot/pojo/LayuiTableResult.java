package cn.yujiago.springboot.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Layui数据表格所需数据格式
 * @param <T>
 */
public class LayuiTableResult<T> implements Serializable {

    // 正常状态为0
    private int code;
    // 消息
    private String msg;
    // 数据（分页数据）
    private List<T> data;
    // 总记录树
    private long count;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
