package cn.yujiago.springboot.pojo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 上传文件后返回前端的实体类
 */
@Builder
@Setter
@Getter
public class FileResult implements Serializable {

    private Integer code;
    private String msg;

}
