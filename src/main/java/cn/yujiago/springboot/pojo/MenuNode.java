package cn.yujiago.springboot.pojo;

import cn.yujiago.springboot.entity.Menu;

import java.io.Serializable;
import java.util.List;

/**
 * 前端动态显示的菜单
 */
public class MenuNode implements Serializable {

    private Long id;
    private String title;
    private String icon;
    private String href;
    private boolean spread;

    private Long parentId;

    private List<MenuNode> children;

    public MenuNode(){
        spread = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public boolean getSpread() {
        return spread;
    }

    public void setSpread(boolean spread) {
        this.spread = spread;
    }

    public List<MenuNode> getChildren() {
        return children;
    }

    public void setChildren(List<MenuNode> children) {
        this.children = children;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
