package cn.yujiago.springboot.pojo;


import cn.yujiago.springboot.entity.RoleDO;

import java.io.Serializable;

/**
 * 用于显示该角色是否被指定用户所拥有
 */
public class RoleNode extends RoleDO implements Serializable {

    // 该角色是否被指定用户拥有
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
