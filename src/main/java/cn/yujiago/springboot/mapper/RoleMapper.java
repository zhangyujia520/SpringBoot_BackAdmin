package cn.yujiago.springboot.mapper;

import cn.yujiago.springboot.entity.RoleDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface RoleMapper extends Mapper<RoleDO> {

    /**
     * 根据用户ID查询该用户所拥有的角色
     * @param userId
     * @return
     */
    List<RoleDO> selectRoles(@Param("userId") Long userId);

    /**
     * 删除该用户ID所拥有的角色
     * @param userId
     * @return
     */
    int deleteUserRoleByUserId(@Param("userId") Long userId);

    /**
     * 为指定用户插入多个角色
     * @param userId
     * @param roleIds
     */
    void insertUserRole(@Param("userId") Long userId, @Param("roleIds") List<Long> roleIds);
}
