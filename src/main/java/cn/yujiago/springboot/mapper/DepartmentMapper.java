package cn.yujiago.springboot.mapper;


import cn.yujiago.springboot.entity.DeptDO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface DepartmentMapper extends Mapper<DeptDO> {

}
