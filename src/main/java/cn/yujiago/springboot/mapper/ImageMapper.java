package cn.yujiago.springboot.mapper;

import cn.yujiago.springboot.entity.Image;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

@Repository
public interface ImageMapper extends Mapper<Image> {

    @Select("select * from image where isShow=0")
    List<Image> selectAllImage();

    @Insert("insert into image(id, title, remark, url, isShow) values(#{image.id},#{image.title},#{image.remark},#{image.url},#{image.isShow})")
    int insertImage(@Param("image") Image image);

    /**
     * 更新图片的isShow字段，0代表展示，1代表不展示，即删除
     * @param list
     * @return
     */
    @UpdateProvider(type = ImageProvider.class, method = "updateImageIsShow")
    int updateImageIsShow(List<String> list);

    class ImageProvider{
        public String updateImageIsShow(Map map){
            List<String > list = (List<String>) map.get("list");

            StringBuilder builder = new StringBuilder();
            builder.append("update image set isShow=1 where id in (");
            if (list != null && list.size() > 0){
                for (int i = 0; i < list.size(); i++) {
                    if(i+1 == list.size()){
                        builder.append("'"+list.get(i)+"'");
                    }else{
                        builder.append("'"+list.get(i)+"', ");
                    }
                }
                return builder.append(")").toString();
            }
            return null;
        }
    }
}
