package cn.yujiago.springboot.mapper;

import cn.yujiago.springboot.entity.UserDO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface UserMapper extends Mapper<UserDO> {

}
