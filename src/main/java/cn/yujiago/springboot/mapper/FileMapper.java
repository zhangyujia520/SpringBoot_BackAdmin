package cn.yujiago.springboot.mapper;

import cn.yujiago.springboot.entity.File;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface FileMapper extends Mapper<File> {

    @Select("select * from file where status=0 order by upload_time desc")
    List<File> selectAllFile();

    @Select("select * from file where status=0 and file_name like CONCAT('%',#{fileName},'%')")
    List<File> selectFileByName(@Param("fileName") String fileName);

    @Insert("insert into file(id, file_name, url, size, upload_time, status) values(#{newFile.id},#{newFile.fileName},#{newFile.url},#{newFile.size},#{newFile.uploadTime},#{newFile.status})")
    int insertFile(@Param("newFile") File newFile);

}
