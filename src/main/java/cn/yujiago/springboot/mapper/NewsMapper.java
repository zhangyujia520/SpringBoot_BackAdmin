package cn.yujiago.springboot.mapper;

import cn.yujiago.springboot.entity.News;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface NewsMapper extends Mapper<News> {

    List<News> selectNewsByCondition(String newsName);

}
