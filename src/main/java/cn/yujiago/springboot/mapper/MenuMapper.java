package cn.yujiago.springboot.mapper;

import cn.yujiago.springboot.entity.MenuDO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface MenuMapper extends Mapper<MenuDO> {

    /**
     * 查询该角色的所有权限
     * @param roleId
     * @return
     */
    List<String> selectPerms(@Param("roleId") Long roleId);

    /**
     * 删除该角色的所有权限
     * @param roleId
     */
    void deleteRoleMenuByRoleId(@Param("roleId") Long roleId);

    /**
     * 插入该角色的全部权限
     * @param roleId
     * @param menuIds
     */
    void insertRoleMenu(@Param("roleId") Long roleId, @Param("menuIds") List<Long> menuIds);
}
