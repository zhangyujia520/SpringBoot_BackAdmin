package cn.yujiago.springboot.exception;

import org.apache.shiro.authc.AuthenticationException;

/**
 * 自定义认证错误异常
 */
public class CustomAuthenticationException extends AuthenticationException {

    // 异常信息
    private String msg;

    public CustomAuthenticationException(String msg){
        super(msg);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
