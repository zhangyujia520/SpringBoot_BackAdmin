package cn.yujiago.springboot.exception;

import cn.yujiago.springboot.pojo.BackAdminResult;
import cn.yujiago.springboot.utils.ExceptionUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

/**
 * 用于捕获和处理Controller抛出的异常
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private final static Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(CustomAuthenticationException.class)
    @ResponseBody
    public BackAdminResult handleCustomAuthentication(Exception ex){
        LOG.info("CustomAuthenticationException handler  " + ex.getMessage() );
        return BackAdminResult.build(1, ex.getMessage());
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseBody
    public BackAdminResult handleAuthentication(Exception ex){
        LOG.info("AuthenticationException handler  " + ex.getMessage() );
        return BackAdminResult.build(1, "认证失败");
    }
}