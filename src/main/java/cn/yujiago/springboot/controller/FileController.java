package cn.yujiago.springboot.controller;

import cn.yujiago.springboot.entity.File;
import cn.yujiago.springboot.pojo.*;
import cn.yujiago.springboot.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileController {

    @Autowired
    private FileService fileService;

    @GetMapping("/file/list")
    public LayuiTableResult<File> getMenuListByCondition(@RequestParam("page") Integer page, @RequestParam("limit") Integer limit, @RequestParam(value = "key", required = false) String key){
        return fileService.getFileByCondition(page, limit, key);
    }

    @RequestMapping("/file/upload")
    public FileResult upload(@RequestParam("file") MultipartFile uploadFile){
        return fileService.uploadFileToQiniuAndInsert(uploadFile);
    }

}
