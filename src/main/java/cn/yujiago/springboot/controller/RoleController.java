package cn.yujiago.springboot.controller;

import cn.yujiago.springboot.entity.RoleDO;
import cn.yujiago.springboot.pojo.LayuiTableResult;
import cn.yujiago.springboot.pojo.ResponseEntity;
import cn.yujiago.springboot.pojo.RoleNode;
import cn.yujiago.springboot.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping("/list")
    public LayuiTableResult<RoleDO> getRolesByCondition(@RequestParam("page") int page, @RequestParam("limit") int limit){
        return roleService.getRolesByCondition(page, limit);
    }

    @RequestMapping("/listByUserId")
    public List<RoleNode> getRolesByUserId(@RequestParam("userId") Long userId){
        return roleService.getRolesByUserId(userId);
    }

    @RequestMapping("/updateUserRole")
    public ResponseEntity updateUserRole(@RequestParam("userId") Long userId, @RequestParam(value = "roleIds[]", required = false) List<Long> roleIds){
        return roleService.updateUserRole(userId, roleIds);
    }
}