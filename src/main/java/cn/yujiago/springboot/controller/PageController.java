package cn.yujiago.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class PageController {

    @GetMapping("/page/{directory}/{filename}")
    public String toPage(@PathVariable("directory") String directory, @PathVariable("filename") String filename){
        return directory+"/"+filename;
    }

}
