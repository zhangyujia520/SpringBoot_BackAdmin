package cn.yujiago.springboot.controller;

import cn.yujiago.springboot.pojo.LayuiTableResult;
import cn.yujiago.springboot.entity.News;
import cn.yujiago.springboot.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewsController {

    @Autowired
    private NewsService newsService;

    @RequestMapping("/news/list")
    public LayuiTableResult<News> getNewsListByCondition(@RequestParam("page") Integer page, @RequestParam("limit") Integer limit, @RequestParam(value = "key", required = false) String key){
        return newsService.getNewsByCondition(page, limit, key);
    }

}
