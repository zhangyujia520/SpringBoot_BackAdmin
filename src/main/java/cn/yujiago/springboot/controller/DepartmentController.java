package cn.yujiago.springboot.controller;

import cn.yujiago.springboot.entity.DeptDO;
import cn.yujiago.springboot.pojo.LayuiTreeNode;
import cn.yujiago.springboot.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dept")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @RequestMapping("/tree")
    public List<LayuiTreeNode<DeptDO>> deptTree(){
        return departmentService.getDepartmentTree();
    }

}
