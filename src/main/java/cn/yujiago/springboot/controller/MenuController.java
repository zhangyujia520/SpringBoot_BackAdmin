package cn.yujiago.springboot.controller;

import cn.yujiago.springboot.entity.MenuDO;
import cn.yujiago.springboot.pojo.AuthNode;
import cn.yujiago.springboot.pojo.LayuiTableResult;
import cn.yujiago.springboot.pojo.MenuNode;
import cn.yujiago.springboot.pojo.ResponseEntity;
import cn.yujiago.springboot.service.MenuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @RequestMapping("/menuTree")
    public List<MenuNode> getMenuTree(){
        return menuService.getMenuTree();
    }

    @RequestMapping("/list")
    public LayuiTableResult<MenuDO> getMenusByCondition(@RequestParam(value = "page", required = false) Long page, @RequestParam(value = "limit", required = false) Long limit){
        if(page == null || limit == null){
            return menuService.getAllMenu();
        }else{
            return menuService.getMenusByCondition(page, limit);
        }
    }

    @RequestMapping("/authTree")
    public LayuiTableResult<AuthNode> getAuthTree(@RequestParam("userId") Long userId){
        return menuService.getAuthTreeByUserId(userId);
    }

    @RequestMapping("/authTreeByRoleId")
    public LayuiTableResult<AuthNode> getAuthTreeByRoleId(@RequestParam("roleId") Long roleId){
        return menuService.getAuthTreeByRoleId(roleId);
    }

    @RequestMapping("/updateRoleMenu")
    public ResponseEntity updateRoleMenu(@RequestParam("roleId") Long roleId, @RequestParam(value = "menuIds[]", required = false) List<Long> menuIds){
        return menuService.updateRoleMenu(roleId, menuIds);
    }
}
