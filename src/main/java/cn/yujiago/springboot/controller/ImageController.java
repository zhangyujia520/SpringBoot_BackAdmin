package cn.yujiago.springboot.controller;

import cn.yujiago.springboot.pojo.BackAdminResult;
import cn.yujiago.springboot.entity.Image;
import cn.yujiago.springboot.pojo.PictureResult;
import cn.yujiago.springboot.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class ImageController {

    @Autowired
    private ImageService imageService;

    @GetMapping("/image/list")
    public BackAdminResult getAllImage(){

        List<Image> images = imageService.getAllImage();

        if(images != null){
            return BackAdminResult.build(200, "查询成功！", images);
        }else{
            return BackAdminResult.build(400, "查询失败！");
        }
    }

//    @PostMapping("/image/uploadToFastDFS")
//    public PictureResult upload(@RequestParam("file") MultipartFile uploadFile){
//        // 上传图片
//        PictureResult result = imageService.uploadImageToFastDFS(uploadFile);
//        // 成功后将图片相关相关信息保存到数据库
//        if(result.getCode() == 0){
//            imageService.saveImageInfo(result.getUrl());
//        }
//        return result;
//    }

    @PostMapping("/image/uploadToQiniu")
    public PictureResult upload(@RequestParam("file") MultipartFile uploadFile){
        // 上传图片
        PictureResult result = imageService.uploadImageToQiniu(uploadFile);
        // 成功后将图片相关相关信息保存到数据库
        if(result.getCode() == 200){
            imageService.saveImageInfo(result.getUrl());
        }
        return result;
    }

    @GetMapping("/image/delete")
    public BackAdminResult deleteImageByIds(@RequestParam("ids") String[] ids){
        return imageService.deleteImageByIds(ids);
    }

}
