package cn.yujiago.springboot.config;

import com.qiniu.util.Auth;

/**
 * 全局常量配置
 */
public interface Constants {

    // 已登录的管理员信息保存在session中的key
    String LOGIN_ADMIN_KEY = "loginAdmin";

    // Durid账号密码
    String DRUID_LOGIN_USERNAME = "admin";
    String DRUID_LOGIN_PASSWORD = "123456";

    // 图片服务器的基础URL，FastDFS+Nginx搭建的图片服务器
    String IMAGE_SERVER_BASE_URL = "http://www.image.yujiago.cn:89/";

    // 七牛云上传文件配置
    // 设置需要操作的账号的AK和SK
    String QINIU_ACCESS_KEY = "DDr8l5LsH52S8t5V8FJIcCq70grOYbgFcuunTpWl";
    String QINIU_SECRET_KEY = "zhorDsHwt15WG7L-KR2CoOYbH24AiaksbdW0eZq0";
    // 要上传的空间
    String QINIU_IMAGE_BUCKER_NAME = "image";
    String QINIU_FILE_BUCKER_NAME = "file";
    // 密钥
    Auth QINIU_AUTH = Auth.create(QINIU_ACCESS_KEY, QINIU_SECRET_KEY);
    // 外域链接
    String QUNIU_IMAGE_DOMAIN = "http://www.qiniu.image.yujiago.cn/";
    String QUNIU_FILE_DOMAIN = "http://www.qiniu.file.yujiago.cn/";
    // 图片裁剪样式
    String QINIU_IMAGE_STYLE = "imageView2/2/w/400/h/400/q/100";

    // 腾讯云短信配置
    int APPID = 1400132234;
    String APPKEY = "eb50b10be02819ebc9f73477d3de19f1";
    // 短信内容模板ID
    int TEMPLATE_ID = 182655;
    // 短信签名
    String SMS_SIGN = "章鱼的blog";

    // 加密算法：MD5、SHA1
    String Hash_Algorithm_Name = "MD5";
    // 散列次数
    int Hash_Iterations = 4;
}
